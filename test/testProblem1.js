const { createDir, crtFile, deleteFiles } = require('../problem1')


function createDirectory(path,callback){
  return callback(path)
}

function createFile(path,callback){
  return callback(path)
}




createDirectory('jsonFiles',createDir);

createFile('./jsonFiles/file1.json', crtFile)
createFile('./jsonFiles/file2.json', crtFile)
createFile('./jsonFiles/file3.json', crtFile)
createFile('./jsonFiles/file4.json', crtFile)
createFile('./jsonFiles/file5.json', crtFile)


const filePath = ["./jsonFiles/file1.json", "./jsonFiles/file2.json", "./jsonFiles/file3.json"]

deleteFiles(filePath, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log('File Deleted');
  }
});
