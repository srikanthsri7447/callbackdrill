const fs = require("fs")
const path = require("path")

//Read File
function readFile(filepath) {
    fs.readFile(filepath, (err) => {
        if (err) {
            console.log(err)
        }
        else {
            console.log("Base File Reading")
        }
    })
}

//Read File And Convert UpperCase

function fileToUpperCase(dataPath, filepath) {

    fs.readFile(path.normalize(dataPath), (err, data) => {
        if (err) {
            console.log(err.message)
        }
        else {

            const upperData = data.toString().toUpperCase()

            fs.writeFile(filepath, upperData, 'utf8', (err) => {
                if (err) {
                    console.log(err.message)
                }
                else {
                    console.log("UpperCase File created")
                }
            })
            const storeFileName = () => {
                fs.writeFile('filenames.txt', filepath + "\n", 'utf8', (err) => {
                    if (err) {
                        console.log(err.message)
                    }
                    else {
                        console.log("UpperCase File Path Stored")
                    }
                })
            }
            setTimeout(storeFileName, 1000)
        }
    })
}

// Convert Into Sentence

function convertSentenceAndStore(dataPath, filepath) {

    fs.readFile(path.normalize(dataPath), (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            console.log('UpperCase File Read')

            let sentence = ''

            const lowerData = data.toString().toLowerCase().split('.')
            lowerData.map((eachSentc) => {
                sentence += eachSentc + "\n"
            })

            fs.writeFile(filepath, sentence, (err) => {
                if (err) {
                    console.log(err)
                }
                else {
                    console.log('Sentences File Created')
                }
            })


            const storeFileName = () => {
                fs.appendFile('filenames.txt', filepath + "\n", 'utf8', (err) => {
                    if (err) {
                        console.log(err.message)
                    }
                    else {
                        console.log("Sentences File Path stored")
                    }
                })
            }
            setTimeout(storeFileName, 1000)

        }
    })

}

//Convert Into Sorted Sentence

function readFIlesInDir(filesPath) {

    const importUpperCase = () => {

        fs.readFile("upperCaseFile.txt", (err, data) => {
            if (err) {
                console.log(err)
            }
            else (
                fs.writeFile(filesPath, data, 'utf-8', (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log('UpperCaseContent Imported')
                    }
                })
            )
        })

    }

    importUpperCase();

    const importSplitedSentence = () => {
        fs.readFile('splitFile.txt', (err, data) => {
            if (err) {
                console.log(err)
            }
            else (
                fs.appendFile(filesPath, data, 'utf-8', (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log('SplitedSentence Imported')
                    }
                })
            )
        })

    }

    setTimeout(importSplitedSentence, 2000)

    const readCombinedFile = () => {

        fs.readFile(filesPath, (err, data) => {
            if (err) {
                console.log(err)
            }
            else {
                console.log('Accessing Combined Data')

                const readCombFile = () => {

                    const sortedArray = data.toString().trim().split("\n").sort()

                    let sentence = ''

                    sortedArray.map((eachSent) => {
                        if (eachSent !== '') {
                            sentence += eachSent + "\n"
                        }
                    })

                    const exportSortedSentence = () => {

                        fs.writeFile(filesPath, sentence, (err) => {
                            if (err) {
                                console.log(err)
                            }
                            else {
                                console.log('Sorted Sentence overWrite')
                            }
                        })
                    }
                    setTimeout(exportSortedSentence, 2000)


                }
                const storeFileName = () => {

                    fs.appendFile('filenames.txt', filesPath + "\n", 'utf8', (err) => {
                        if (err) {
                            console.log(err.message)
                        }
                        else {
                            console.log("Sorted Sentence Path Stored")
                        }
                    })
                }
                setTimeout(storeFileName, 4000)

                setTimeout(readCombFile, 1000)
            }
        })

    }

    setTimeout(readCombinedFile, 4000)



}

//Delete All New Files

function deleteFiles(filesPath) {
    fs.readFile(path.join(filesPath, ""), (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            console.log('Stored Paths Read')

            const array = data.toString().trim().split("\n")

            const deleteFn = () => {
                array.map((eachFile) => {
                    fs.unlink(eachFile, (err) => {
                        if (err) {
                            console.log(err);

                        }
                        else {

                            console.log(`${eachFile} is Deleted`);

                        }
                    });
                })
            }
            setTimeout(deleteFn, 1000)
        }

    })
}

module.exports = { readFile, fileToUpperCase, convertSentenceAndStore, readFIlesInDir, deleteFiles };