const fs = require('fs')
const path = require("path");



//Create Directory 

function createDir(dirName) {
    fs.mkdir(path.join(__dirname, dirName), (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('Directory Created')
        }
    })
}


//Create File in Directory

function crtFile(filePath) {
    fs.writeFile(path.join(filePath, ''), 'Hello World!', 'utf8', (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('File Created')

        }
    })
}

// Delete file in Directory

function deleteFiles(files, callback) {
    const deleteFn = () => {

        files.map((eachFile) => {
            fs.unlink(eachFile, (err) => {

                if (err) {
                    callback(err);
                    return;
                }
                else {
                    callback(null);
                }
            });

        })
    }
    setTimeout(deleteFn, 1000)
}


module.exports = { createDir, crtFile, deleteFiles };
